﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Src.Interfaces;

namespace Assets.Src.Classes
{
    public class HealthClass : IHealth
    {

        float _maxHealth = 0;
        float _currentHealth = 0;

        public HealthClass()
        {
            _maxHealth = 100;
            _currentHealth = _maxHealth;

        }

        public HealthClass(float maxHealth)
        {

            _maxHealth = maxHealth;
            _currentHealth = _maxHealth;
        }

        public float GetCurrentHealth()
        {
            return _currentHealth;
        }

        public float GetMaxHealth()
        {
            return _maxHealth;
        }

        public void Set(float val) {

            if (val < 0)
            {
                _currentHealth = 0;
                return;
            }

            if (val >= _maxHealth)
            {
                _currentHealth = _maxHealth;
                return;
            }

            _currentHealth = val;

        }


        public void Increase(float val)
        {
            float newHealthVal = _currentHealth + val;
            Set(newHealthVal);

        }
        public void Decrease(float val)
        {

            float newHealthVal = _currentHealth - val;
            Set(newHealthVal);
        }

    }
}
