﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Src.Interfaces;

namespace Assets.Src.Classes
{
    public class CharacterClass : ICharacter
    {

        IItem ItemSlot = null;
        public IHealth HealthBar = null;


        public CharacterClass(float healthPoints = 100)
        {
            HealthBar = new HealthClass(healthPoints);
        }

        /*
        public CharacterClass(bool isItAlive = false, float healthPoints = 100)
        {

            if (isItAlive)
            {
                HealthBar = new HealthClass(healthPoints);
            }

        }*/

        private bool HasItem()
        {
            return (this.ItemSlot != null) ? true : false;
        }

        private bool HasHealthBar()
        {
            return (this.HealthBar != null) ? true : false;
        }


        // ------------------------------------------------


        // TODO : refactor these methods
        public virtual void Move(float h, float v)
        {
        }

        public virtual void Jump()
        {
        }

        public void Rotate(float x, float y, float z, float w)
        {


        }

        public virtual void Animate()
        {
        }


        public virtual bool Pick(IItem levelItemInstance) {

            this.ItemSlot = levelItemInstance;
            return HasItem();
        }

        public virtual bool Drop() {


            // TODO : recheck later
            this.ItemSlot = null;
            return !HasItem();
        }

        public virtual bool Activate() {


            return false;
        }


        public bool isDead()
        {
            // if (HasHealthBar() && (HealthBar.GetCurrentHealth() <= 0f))
            //     return true;

            if (HealthBar.GetCurrentHealth() <= 0f)
            {
                return true;
            }

            return false;

        }



        public virtual void Action() {
            
            if (HasItem())
            {
                ItemSlot.Action();
            }

        }


        // Die (in this case)
        public virtual void Destroy() {

            // if (HasHealthBar()) {}
            HealthBar.Set(0);
        }


    }
}
