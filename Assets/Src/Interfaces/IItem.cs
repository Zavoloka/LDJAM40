﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Src.Interfaces
{

    public interface IItem
    {

        void Action();
        void Destroy();
    
    }


}

