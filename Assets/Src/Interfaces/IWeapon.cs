﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Src.Interfaces
{
    public interface IWeapon
    {
        void Attack(float damage);
    }
}
