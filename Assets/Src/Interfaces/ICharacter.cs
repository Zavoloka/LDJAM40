﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Src.Interfaces
{
    public interface ICharacter : IItem
    {

        void Move(float h, float v);
        void Rotate(float x, float y, float z, float w);
        void Jump();
        void Animate();
        bool Pick(IItem levelItem);
        bool Drop();
        bool Activate();
        bool isDead();


    }
}
