﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Src.Interfaces
{
    public interface IHealth
    {

        void Set(float val);
        void Increase(float val);
        void Decrease(float val);
        float GetCurrentHealth();
        float GetMaxHealth();


    }
}
