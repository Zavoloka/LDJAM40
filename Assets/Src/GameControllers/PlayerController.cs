﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Src.Classes;
using Assets.Src.Interfaces;
using Assets.Src.StaticClasses;
using UnityEngine;

namespace Assets.Src.GameControllers
{
    class PlayerController : CharacterGameController
    {

        bool _isGounded = false;

 
        bool _nearItem = false;
        GameObject _nearItemStorage = null;

        protected List<GameObject> _weaponGameObjects;
        protected int _weaponLayer;
        int _childCount;

        int _enemyLayer;
        int _bulletLayer;

        public float  MaxHealth = 200;




        protected virtual void Awake()
        {

            _weaponGameObjects = new List<GameObject>();
            _weaponLayer = LayerMask.NameToLayer("Weapon");
            _childCount = transform.childCount;
            foreach (Transform child in transform)
            {
                if (child.gameObject.layer == _weaponLayer)
                {
                    _weaponGameObjects.Add(child.gameObject);
                }
            }

           // Debug.Log("Awake here");
           // Debug.Log("child count : " + _childCount);
           // Debug.Log("Weapon check : " + _weaponGameObjects);
        }




        protected override void Start()
        {


            base.Start();
            base._characterClassComponent.HealthBar = new HealthClass(MaxHealth);
         
            _enemyLayer = LayerMask.NameToLayer("Enemy");
            _bulletLayer = LayerMask.NameToLayer("Bullet");

        }


        protected override void Update()
        {
            /*
            // input data
            float v = Input.GetAxis("Vertical");
            float h = Input.GetAxis("Horizontal");
            float j = (Input.GetButton("Jump")) ? 1f : 0f;
            //bool e = Input.GetKey(KeyCode.E); //  pick/drop item
           // e = false;

            bool a = Input.GetButtonDown("Fire1");
            //a = (Input.GetButton("Fire1")) ? true : a;


            //Debug.Log("A check : " + a);

            //Debug.Log("Inputs : " + v.ToString() + " / " + h.ToString() + " / " + j.ToString());
            base.Move(h, v);


            // attack
            if (a)
            {
                //StartCoroutine(AttackAll());
                AttackAll();
            
            }


            // mouse rotation
            // -------------------------------------------------
            Vector3 currentMousePosition = Input.mousePosition;
            Ray camRay = Camera.main.ScreenPointToRay(currentMousePosition);
            RaycastHit envHit;
            // TODO : check static layer later
            bool envHitCheck = Physics.Raycast(camRay, out envHit, 500, LayerMask.GetMask(new string[] {"Floor", "Wall"}));
            //Debug.Log("Env hit check : " + envHitCheck.ToString());
            if (envHitCheck)
            {
                Vector3 playerToMouse = envHit.point - base._rigidBodyComponent.position;
                playerToMouse.y = 0f;

               // Debug.Log("Rotation Inputs: " + playerToMouse.x.ToString() + " / " + playerToMouse.y.ToString() + " / " + playerToMouse.z.ToString());
                base.Rotate(playerToMouse.x, playerToMouse.y, playerToMouse.z, 0f);

            }

            // jump
            // -------------------------------------------------
            base.Jump(_isGounded, j);


            //CharacterController cc = GetComponent<CharacterController>();
            //Debug.Log("Custom isGrounded : " + _isGounded.ToString());




            // pick item (unused for now)
            // if (e)
            // {
            // ItemHandler();
            // }


            //Debug.Log("player health : " + base._characterClassComponent.HealthBar.GetCurrentHealth());
            */
        }


        protected void FixedUpdate()
        {

            // input data
            float v = Input.GetAxis("Vertical");
            float h = Input.GetAxis("Horizontal");
            float j = (Input.GetButton("Jump")) ? 1f : 0f;
            //bool e = Input.GetKey(KeyCode.E); //  pick/drop item
            // e = false;

            bool a = Input.GetButtonDown("Fire1");
            //a = (Input.GetButton("Fire1")) ? true : a;


            //Debug.Log("A check : " + a);

            //Debug.Log("Inputs : " + v.ToString() + " / " + h.ToString() + " / " + j.ToString());
            base.Move(h, v);


            // attack
            if (a)
            {
                //StartCoroutine(AttackAll());
                AttackAll();

            }


            // mouse rotation
            // -------------------------------------------------
            Vector3 currentMousePosition = Input.mousePosition;
            Ray camRay = Camera.main.ScreenPointToRay(currentMousePosition);
            RaycastHit envHit;
            // TODO : check static layer later
            bool envHitCheck = Physics.Raycast(camRay, out envHit, 500, LayerMask.GetMask(new string[] { "Floor", "Wall" }));
            //Debug.Log("Env hit check : " + envHitCheck.ToString());
            if (envHitCheck)
            {
                Vector3 playerToMouse = envHit.point - base._rigidBodyComponent.position;
                playerToMouse.y = 0f;

                // Debug.Log("Rotation Inputs: " + playerToMouse.x.ToString() + " / " + playerToMouse.y.ToString() + " / " + playerToMouse.z.ToString());
                base.Rotate(playerToMouse.x, playerToMouse.y, playerToMouse.z, 0f);

            }

            // jump
            // -------------------------------------------------
            base.Jump(_isGounded, j);


            //CharacterController cc = GetComponent<CharacterController>();
            //Debug.Log("Custom isGrounded : " + _isGounded.ToString());




            // pick item (unused for now)
            // if (e)
            // {
            // ItemHandler();
            // }


            //Debug.Log("player health : " + base._characterClassComponent.HealthBar.GetCurrentHealth());
        }

        private void ItemHandler()
        {

            return;
            //Debug.Log("Inside Item handler");
            if (base.Item != null)
            {
                base.Drop();
                return;
            }

       
            bool isNearItemStorageEmpty = (_nearItemStorage == null) ? true : false ;
            Debug.Log("near item : " + _nearItem + " item storage : " + isNearItemStorageEmpty);
            if (_nearItem && !isNearItemStorageEmpty)
            {
                base.Pick(_nearItemStorage);
            }

        }

        protected virtual void AttackAll()
        {


            //Debug.Log("Attack all");
            //Debug.Log("child count : " + _childCount);
            //Debug.Log("Weapon check : " + _weaponGameObjects);


            //  for (int i = 0; i < _chi)

            foreach (GameObject weapon in _weaponGameObjects)
            {
                weapon.GetComponent<PlayerWeaponController>().Action(); ;
                // PlayerWeaponController pwc = weapon.GetComponent<PlayerWeaponController>().Action(); ;
                //pwc.Action();
            }
            //yield return new WaitForSeconds(0);

        }

        protected virtual IEnumerator AttackAll(bool flag = false)
        {


            //Debug.Log("Attack all");
            //Debug.Log("child count : " + _childCount);
            //Debug.Log("Weapon check : " + _weaponGameObjects);


            //  for (int i = 0; i < _chi)

             foreach (GameObject weapon in _weaponGameObjects)
              {
                 weapon.GetComponent<PlayerWeaponController>().Action(); ;
            // PlayerWeaponController pwc = weapon.GetComponent<PlayerWeaponController>().Action(); ;
            //pwc.Action();
               }
            yield return new WaitForSeconds(0);

        }


        private void OnCollisionEnter(Collision collisionObject)
        {

         
            GameObject collisionGameObject = collisionObject.gameObject;
            int collisionLayer = collisionGameObject.layer;

            // For Jumps
            if (collisionLayer == 8)
            {
                _isGounded = true;

            }

            // ------------------------------------------

            // Bullets (Enemies)
            //HandleEnemyCollisions(ref collisionLayer, ref collisionGameObject);


        }

        /*
        private void HandleEnemyCollisions(ref int collisionLayer, ref GameObject other)
        {

            /*(collisionLayer == _enemyLayer) ||*/
        /*
            if ((collisionLayer == _bulletLayer) )
            {
                BulletController bc = other.GetComponent<BulletController>();
                float damage = 5;
                if (bc != null)
                {
                    damage = bc.BulletDamage;
                }

                Debug.Log("Inside HandleEnemyCollisions ");
                base.DecreaseHitPoints(damage);
            }

        }*/



        private void OnCollisionExit(Collision collisionObject)
        {

            if (collisionObject.gameObject.layer == 8)
            {
                _isGounded = false;
            }
        }




        private void OnTriggerEnter(Collider other)
        {

           
           //bool itemCheck = CheckTriggerForItem(ref other);
           /* item code
           Debug.Log("OntriggerEnter");
           Debug.Log("item check : " + itemCheck); 

            if (itemCheck)
            {
                // enter to Item trigger
                _nearItem = true;
                _nearItemStorage = other.transform.parent.gameObject;
            }
            */



        }


        private void OnTriggerExit(Collider other)
        {
            //bool itemCheck = CheckTriggerForItem(ref other);
            /* item code
            if (itemCheck)
            {
                // leaving item trigger
                _nearItem = false;
                _nearItemStorage = null;
            } */

        }

        private bool CheckTriggerForItem(ref Collider other)
        {

            return false;
            Debug.Log("other layer : " + other.gameObject.layer);
            Debug.Log("item layer : " + LayerMask.NameToLayer("Item"));
            if (other.gameObject.layer != LayerMask.NameToLayer("Item"))
            {        
                return false;
            }

            if (other.gameObject.transform.parent.gameObject.GetComponent<ItemController>() == null)
            {
                return false;
            }
            Debug.Log("pass");

            return true;
        }

    }
}
