﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Src.Classes;
using Assets.Src.Interfaces;
using UnityEngine;
using UnityEngine.UI;


namespace Assets.Src.GameControllers
{
    public class CharacterGameController : MonoBehaviour, ICharacter
    {

        public float MovementSpeed = 1f;
        public float RotationSpeed = 1f;
        public float JumpForce = 5f;
        //private float _verticalVelocity = 1f;

        public GameObject Item;


        protected Rigidbody _rigidBodyComponent = null;
        protected Animation _animationComponent = null;

        protected CharacterClass _characterClassComponent = null;

        protected RectTransform _childCanvas;
        protected Text _childHealthText;
        protected Vector3 _childCanvasRotation;

        protected AudioSource _audioSourceComponent;

        //protected Camera _mainCamera;

        //protected Canvas

        // protected List<GameObject> _weaponGameObjects; // TODO later
        // protected int _weaponLayer;


        public Material CurrentMaterial;
        public float HurtSeconds;
        public Material HurtMaterial;
        Renderer _currentRendrer;
        public Renderer RewriteRenderer;



        // Use this for initialization
        protected virtual void Start()
        {
            _rigidBodyComponent = GetComponent<Rigidbody>();
            //_animationComponent = GetComponent<Animation>();

            _characterClassComponent = new CharacterClass(100);

            //_mainCamera = GameObject.Find(""

            _currentRendrer = gameObject.GetComponent<Renderer>();
            if ((_currentRendrer == null) && (RewriteRenderer != null))
            {
               // Debug.Log("Rwrite mat success : " + _currentRendrer);
                _currentRendrer = RewriteRenderer;
            }


            // get AudioSource
            _audioSourceComponent = gameObject.GetComponent<AudioSource>();

  



            //Debug.Log("Current material : " + CurrentMaterial);


            InitUI();



            /*
            List<GameObject> _weaponGameObjects = new List<GameObject>();
            _weaponLayer = LayerMask.NameToLayer("Weapon");
            int childCount = transform.childCount;
            foreach (Transform child in transform)
            {
                if (child.gameObject.layer == _weaponLayer)
                {
                    _weaponGameObjects.Add(child.gameObject);
                }
            }*/

        }


        protected virtual void InitUI()
        {

            _childCanvas = GetComponentInChildren<RectTransform>();
            if (_childCanvas != null)
            {
                _childHealthText = _childCanvas.GetComponentInChildren<Text>();
            }
            

           //_childCanvasRotation = _childCanvas.rotation.eulerAngles;
        }

        // Update is called once per frame
        protected virtual void Update()
        {

            //_childCanvasRotation = _childCanvas.rotation;
        }


        protected virtual void LateUpdate()
        {

            if (_childHealthText != null)
            {
                UpdateHealthUI();
            }
            
        }

        protected virtual void UpdateHealthUI()
        {

            _childHealthText.text = _characterClassComponent.HealthBar.GetCurrentHealth().ToString()
                                   + "/" + _characterClassComponent.HealthBar.GetMaxHealth().ToString();

            Vector3 mainCameraPosition = Camera.main.transform.position;
            //Debug.Log("Main camera position : " + mainCameraPosition);
            mainCameraPosition.y = 0;
        } 

        protected virtual void FixedUpdate()
        {
            
        }


        //private onTrigger/onCollision


        public virtual void Move(float h, float v)
        {

            Vector3 moveVector = new Vector3(h, 0, v);
            _rigidBodyComponent.AddForce(moveVector.normalized * MovementSpeed, ForceMode.VelocityChange);

            //Vector3 newPosition = transform.position + moveVector.normalized * MovementSpeed * Time.deltaTime;
            //_rigidBodyComponent.MovePosition(transform.position);

        }

        public void Jump()
        {
          // TODO : fix later
        }

        public virtual void Jump(bool isGrouded, float j)
        {


            // Debug.Log("Jump : " + j.ToString());
            //float temporaryVerticalVelocity = _verticalVelocity - Physics.gravity.sqrMagnitude * Time.deltaTime;

            Vector3 jumpVector = new Vector3(0f, JumpForce * j, 0f); 
            if (isGrouded)
            {
               // Debug.Log("Inside jump grounded");
               // Debug.Log("j here : " + j.ToString());
               _rigidBodyComponent.AddForce(jumpVector, ForceMode.Acceleration);
              // transform.position()

            }
            
            //Vector3 jumpVector = new Vector3(0, _verticalVelocity, 0);
            //Debug.Log(" Vertical Velocity : " + _verticalVelocity);

            
        }

       
        public virtual void Rotate(float x, float y, float z, float w)
        {
            Quaternion rotationQuaterion = Quaternion.LookRotation(new Vector3(x, y ,z) * RotationSpeed);
            _rigidBodyComponent.MoveRotation(rotationQuaterion);
       

        }

        protected IEnumerator Rotate(Vector3 rotationVector, float rotationSpeed, float waitTime)
        {
            yield return new WaitForSeconds(0.25f);
            while (true)
            {
                transform.Rotate(rotationVector, rotationSpeed * Time.deltaTime);
                yield return new WaitForSeconds(waitTime);
            }

        }
/*         
 *       don't have time for this : )
 *       
        protected IEnumerator MoveWorld(Vector3 moveVector, float moveSpeed, float waitTime)
        {
            yield return new WaitForSeconds(2);
            Vector3 originalPosition = transform.position;
            Vector3 destination = originalPosition + moveVector;

            float x = 0;
            float y = 0;
            float z = 0;

            while (true)
            {

                

                Vector3 newMoveVector = new Vector3(
                                                    Mathf.PingPong(x, moveVector.x), 
                                                    Mathf.PingPong(y, moveVector.y),
                                                    Mathf.PingPong(z, moveVector.z)
                                                    
                                                    );
                transform.position()
              
            }

        } */


        /*
                protected IEnumerator MoveWorld(Vector3 moveVector, float moveSpeed, float waitTime)
                {
                    yield return new WaitForSeconds(2);
                    Vector3 originalPosition = transform.position;
                    Vector3 destination = originalPosition + moveVector;
                    Vector3 currentPosition = originalPosition;
                    Vector3 delta = new Vector3(0.2f, 0.2f, 0.2f);

                    while (true)
                    {
                        Vector3 new D
                        while ( (destination - currentPosition) >  delta)
                        {


                        }


                        yield return new WaitForSeconds(waitTime);

                        transform.Rotate(rotationVector, rotationSpeed * Time.deltaTime);
                        yield return new WaitForSeconds(waitTime);
                    }

                }
        */
        protected void MoveToVector(Vector3 currentPosition, Vector3 destination) 
        {
            transform.position  += (destination - currentPosition) * Time.deltaTime;
        }



        public virtual void Animate()
        {

        }

        public virtual bool Pick(IItem levelItem)
        {

       
            return false;
        }


        public virtual bool Pick(GameObject nearestItem)
        {

            Debug.Log("inside pick!");
            // check if game object is pickable and fill inner item slot
            nearestItem.transform.parent = gameObject.transform;
            Item = nearestItem;


            // perform transform 
           // Item.transform.position = gameObject.transform.position + Vector3.up * 0.5f + Vector3.forward * 0.5f;
          

            //_characterClassComponent.Pick;

            return false;
        }



        public virtual bool Drop()
        {

            if (Item == null)
            {
                return true;
            }
            Item.transform.parent = null;
            Item = null;

            return true;
        }






        public virtual bool Activate()
        {
            return false;
        }

        public void DecreaseHitPoints(float val)
        {

            _characterClassComponent.HealthBar.Decrease(val);
            //Debug.Log("current health " + _characterClassComponent.HealthBar.GetCurrentHealth() + " val " + val );

            if (_audioSourceComponent != null)
            {
                _audioSourceComponent.Play();
            }
          
            StartCoroutine(HurtMaterialAnimation());
            if (isDead())
            {
                Destroy();
                // Game Over / Show Some text / Restart level
            }
                  
        }


        protected IEnumerator HurtMaterialAnimation()
        {
            if (HurtMaterial != null)
            {
               
                if (_currentRendrer == null)
                {

                   // Debug.Log("current renderer test + " + _currentRendrer);
                    StopCoroutine(HurtMaterialAnimation());
                    
                }
                _currentRendrer.material = HurtMaterial;

                yield return new WaitForSeconds(HurtSeconds);


                if (CurrentMaterial != null)
                {
                    _currentRendrer.material = CurrentMaterial;
                }
    
             }

        }

        public virtual bool isDead()
        {
            return (_characterClassComponent.HealthBar.GetCurrentHealth() <= 0f) ? true : false;
        }



        public virtual void Action()
        {

        }

        public virtual void Destroy()
        {
            Destroy(gameObject, 2);
        }
    }
}
