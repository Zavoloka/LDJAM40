﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Src.GameControllers
{

    public class CameraFollower : MonoBehaviour
    {

        public Transform Target;
        public float Smoothing = 5f;
        private Vector3 _cameraOffset;

        // Use this for initialization
        void Start()
        {

            this._cameraOffset = this.transform.position - this.Target.position;
            this._cameraOffset.y = 0;
        }

        // Update is called once per frame
        void Update()
        {


            if (this.Target == null)
            {
                return;
            }

            Vector3 playerPos = this.Target.position;
            playerPos.y = transform.position.y;

            Vector3 targetCamPos = playerPos + this._cameraOffset;

            this.transform.position = Vector3.Lerp(this.transform.position, targetCamPos,
                                                    this.Smoothing * Time.deltaTime);

        }
    }

}
