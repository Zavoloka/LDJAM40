﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using Assets.Src.Interfaces;



namespace Assets.Src.GameControllers
{
    class SmallEnemyController : CharacterGameController, IWeapon
    {

        public float BoxRotationSpeed = 10f;
        public float BoxRotationWaitTime = 0.2f;
        public Vector3 RotationVector = Vector3.up;

        private Transform _playerTransform;
        private NavMeshAgent _nav;



        protected override void Start()
        {
            base.Start();
            StartCoroutine(base.Rotate(RotationVector, BoxRotationSpeed, BoxRotationWaitTime));

            _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            _nav = GetComponent<NavMeshAgent>();
        }

        protected override void Update()
        {
            // Debug.Log("Enemy health : " +  base._characterClassComponent.HealthBar.GetCurrentHealth());

            if ((_playerTransform != null) && (_nav != null) && (_nav.enabled))
            {
                _nav.SetDestination(_playerTransform.position);
            }
        }



      
        public void Attack(float damage)
        {

        }



    }
}
