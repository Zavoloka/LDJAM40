﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Src.GameControllers
{
    public class PlayerWeaponController : WaponController
    {


        // Use this for initialization
        override protected void Start()
        {


        }

        // Update is called once per frame
        void Update()
        {

        }

        // contains Action (base class)

        public void Action()
        {

            var bulletInstance = Instantiate(Resources.Load("Prefabs/PlayerBulletPrefab", typeof(GameObject)), transform.position, transform.rotation) as GameObject;
            BulletController bc = bulletInstance.GetComponent<BulletController>();
            bc.WeaponPosition = transform.position;
            bc.EnemyPosition = transform.parent.transform.position;

            //Debug.Log("Weapon position : " + bc.WeaponPosition);
           // Debug.Log("Enemy Position : " + bc.EnemyPosition);
            // set bullet as weapon's child
            //bulletInstance.transform.parent = gameObject.transform;

            //BulletController bc = bulletInstance.AddComponent<BulletController>();

            // Resources.Load("BulletPrefab")
            //Debug.Log("Inside Instance");
        }


    }
}
