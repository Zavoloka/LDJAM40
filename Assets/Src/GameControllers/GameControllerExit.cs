﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControllerExit : MonoBehaviour {


    public string NewLevel = "debugLevel";
    //public bool CheckForPlayer = true;
    public float NewLevelWait = 3f;
    public float RestartLevelWait = 3f;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {

        if (!other.gameObject.CompareTag("Player"))
        {
            return;
        }

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("BossTag");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        //bool forceNextLevel = false;
        // if ((enemies.Length == 0) && (player == null))
        // {
        //    forceNextLevel = true;
        // }

        if ((enemies.Length == 0))
        {

            StartCoroutine(LoadLevelAfterWait(NewLevelWait));
        }

        if (players.Length == 0)
        {

            StartCoroutine(RestartLevelAfterWait(RestartLevelWait));
        }

    }


    IEnumerator LoadLevelAfterWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        SceneManager.LoadScene(NewLevel);
    }

    IEnumerator RestartLevelAfterWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
