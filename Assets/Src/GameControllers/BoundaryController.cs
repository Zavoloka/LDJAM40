﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Src.GameControllers
{
    public class BoundaryController : MonoBehaviour
    {


        private void OnTriggerExit(Collider other)
        {
          //  if (other.CompareTag("Player")){

                // restart scene
            //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //}

            Destroy(other.gameObject);
        }
    }
}