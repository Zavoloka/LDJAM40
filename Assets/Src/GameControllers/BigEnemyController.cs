﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Assets.Src.Interfaces;
using Assets.Src.Classes;


namespace Assets.Src.GameControllers
{
    class BigEnemyController : CharacterGameController, IWeapon
    {

        public float BoxRotationSpeed = 10f;
        public float BoxRotationWaitTime = 0.2f;
        public Vector3 RotationVector = Vector3.up;

        public float MaxHealth = 400f;



        protected override void Start()
        {
            base.Start();
            //StartCoroutine(base.Rotate(RotationVector, BoxRotationSpeed, BoxRotationWaitTime));
            base._characterClassComponent.HealthBar = new HealthClass(MaxHealth);

            //Debug.Log("check base rendrer" + base.RewriteRenderer);
            //Debug.Log("check base rendrer material " + base.RewriteRenderer.material);
        }

        //
        /*
        protected override void Update()
        {
            // Debug.Log("Enemy health : " +  base._characterClassComponent.HealthBar.GetCurrentHealth());
        }*/


        public void Attack(float damage)
        {

        }



    }
}