﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Src.Interfaces;

namespace Assets.Src.GameControllers
{
    public class BulletController : MonoBehaviour, IItem
    {

        public float BulletSpeed = 20f;
        public float LifeTime = 5f;
        public Vector3 WeaponPosition = new Vector3(0, 0, 0);
        public Vector3 EnemyPosition = new Vector3(0, 0, 0);
        private Rigidbody _rigidBodyComponent = null;
        public float BulletDamage = 5f;

        protected int _enemyLayer;
        protected int _boundaryLayer;
        protected int _weaponLayer;
        protected int _bulletLayer;


        public string _aimTag = "Player";
        public string SelfIgnoreLayer = "Enemy";

        //protected AudioSource _audioSourceComponent;



        protected virtual void Awake()
        {
            _enemyLayer = LayerMask.NameToLayer(SelfIgnoreLayer);
            _boundaryLayer = LayerMask.NameToLayer("Boundary");
            _weaponLayer = LayerMask.NameToLayer("Weapon");
            _bulletLayer = LayerMask.NameToLayer("Bullet");

            // get AudioSource
           // _audioSourceComponent = gameObject.GetComponent<AudioSource>();
        }

        protected virtual void Start()
        {

            _rigidBodyComponent = GetComponent<Rigidbody>();
            //StartCoroutine(MoveBullet());

            // if (_audioSourceComponent != null)
           // {
           //     _audioSourceComponent.Play();
          //  }
            Destroy(gameObject, LifeTime);


        }

        protected virtual void Update()
        {

            //Vector3 newTransformVector = WeaponPosition - EnemyPosition;
            // transform.Translate(newTransformVector * Time.deltaTime * BulletSpeed);
           // _rigidBodyComponent.AddForce(newTransformVector * Time.deltaTime * BulletSpeed, ForceMode.VelocityChange);

            //Debug.Log("Weap pos : " + WeaponPosition + " / Enemy pos : " + EnemyPosition);
            //Debug.Log("Bullet Vector : " + newTransformVector);
           //Debug.Log("Test pos : " + transform.position);

        }

        /*
        IEnumerator MoveBullet()
        {
            while (true)
            {
                //transform.Translate(Vector3.forward * Time.deltaTime * BulletSpeed);
                Vector3 newTransformVector = WeaponPosition - EnemyPosition;
                _rigidBodyComponent.AddForce(newTransformVector * BulletSpeed, ForceMode.Acceleration);
                Debug.Log("Test pos : " + newTransformVector);

            }
            yield return new WaitForSeconds(0.01f);


        }*/

        protected void FixedUpdate()
        {
            Vector3 newTransformVector = WeaponPosition - EnemyPosition;
            // transform.Translate(newTransformVector * Time.deltaTime * BulletSpeed);
            _rigidBodyComponent.AddForce(newTransformVector * Time.deltaTime * BulletSpeed, ForceMode.VelocityChange);

        }


        protected virtual void OnCollisionEnter(Collision collision)
        {
            GameObject collisionGameObject = collision.gameObject;
            int otherLayer = collisionGameObject.layer;

            PlayerCheck(ref collisionGameObject);
            DoNotDestroyLayerCheck(ref otherLayer);
        }

        protected virtual void OnTriggerEnter(Collider other)
        {

            GameObject collisionGameObject = other.gameObject;
            int otherLayer = collisionGameObject.layer;

            PlayerCheck(ref collisionGameObject);
            DoNotDestroyLayerCheck(ref otherLayer);


        }

        protected virtual void PlayerCheck(ref GameObject other)
        {

            //Debug.Log("inside player check");
            //Debug.Log("Inside PlayerCheck");
            if (other.CompareTag(_aimTag))
            {
                //Debug.Log("Aim!" + _aimTag);
                Transform nodeTransform = other.transform;
                Transform rootTransform = other.transform.root;
         
                CharacterGameController rootCGC = rootTransform.GetComponent<CharacterGameController>();
                CharacterGameController nodeCGC = nodeTransform.GetComponent<CharacterGameController>();
                CharacterGameController rootchildCGC = rootTransform.GetComponentInChildren<CharacterGameController>();

                if (nodeCGC != null)
                {
                    nodeCGC.DecreaseHitPoints(BulletDamage);
                    return;
                }

                //Debug.Log("check root CGC : " + rootCGC);
                if (rootCGC != null)
                {
                    rootCGC.DecreaseHitPoints(BulletDamage);
                    return;

                }

                if (rootchildCGC != null)
                {
                    rootchildCGC.DecreaseHitPoints(BulletDamage);
                    return;

                }

                //other.GetComponent<CharacterGameController>().DecreaseHitPoints(BulletDamage);
            }
        }

        protected virtual void DoNotDestroyLayerCheck(ref int otherLayer)
        {
            if ((otherLayer != _enemyLayer) && (otherLayer != _weaponLayer) && (otherLayer != _boundaryLayer) && (otherLayer != _bulletLayer))
            {
                //Debug.Log("This bullet - inside destroy");
                Destroy(gameObject);
            }

        }

        protected virtual void OnTriggerExit(Collider other)
        {
            // 9 - boundary
            // if (other.gameObject.layer == 9)
            //{
            //  Destroy(gameObject);
            // }

        }


        public void Action()
        {


        }
        public void Destroy()
        {


        }


    }
}