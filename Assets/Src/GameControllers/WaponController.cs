﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Src.Interfaces;

namespace Assets.Src.GameControllers
{
    public class WaponController : MonoBehaviour, IItem
    {

        public float BulletFrequency = 20;
        public float BulletWaitTime = 0.05f;
        public string PrefabPath = "Prefabs/BulletPrefab";

       // public GameObject Bullet;



        protected virtual void Start()
        {
            StartCoroutine(Shooting());
        }

        protected virtual void Update()
        {
            
        }

        IEnumerator Shooting()
        {
            //yield return new WaitForSeconds(3f);
            while (true)
            {
                yield return new WaitForSeconds(BulletWaitTime * BulletFrequency);
                Action();
 
            }

        }

        public void Action()
        {

            var bulletInstance = Instantiate(Resources.Load(PrefabPath, typeof(GameObject)), transform.position, transform.rotation) as GameObject;
            BulletController bc = bulletInstance.GetComponent<BulletController>();
            bc.WeaponPosition = transform.position;
            bc.EnemyPosition = transform.parent.transform.position;
        
            // set bullet as weapon's child
            //bulletInstance.transform.parent = gameObject.transform;



            //BulletController bc = bulletInstance.AddComponent<BulletController>();

            // Resources.Load("BulletPrefab")
            //Debug.Log("Inside Instance");
        }
        public void Destroy()
        {


        }
    }
}
